<?php
/**
 *
 * @package WordPress
 * @subpackage dmresponsive
 * @since DM-Responsive 1.0
 */
?>
<?php get_header(); ?>
 
<div class="main wrapper clearfix">
 		
	<section class="content-main">

<?php the_post(); ?>
 
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <header>
                    	<h1 class="entry-title"><?php the_title(); ?></h1>
                    </header>
                    
					 <?php the_content(); ?>
				  
                </article>          
 
	</section>
			
			<?php get_sidebar(); ?>
			
            </div><!-- #main -->
        </div><!-- #main-container -->
 <?php get_footer(); ?>