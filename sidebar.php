<?php if ( is_sidebar_active('primary_widget_area') ) : ?>
        <aside class="widget-area">
            <div class="padder">
            <ul class="xoxo">
                <?php dynamic_sidebar('primary_widget_area'); ?>
            </ul>
            </div>
        </aside><!-- #primary .widget-area -->
<?php endif; ?>       
 
<?php if ( is_sidebar_active('secondary_widget_area') ) : ?>
        <aside class="widget-area">
            <div class="padder">
            <ul class="xoxo">
                <?php dynamic_sidebar('secondary_widget_area'); ?>
            </ul>
            </div>
        </aside><!-- #secondary .widget-area -->
<?php endif; ?>