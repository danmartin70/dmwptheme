<?php
/**
 *
 * @package WordPress
 * @subpackage dmresponsive
 * @since DM-Responsive 1.0
 *
 */

?><!DOCTYPE html>
<!--[if lt IE 7 ]><html <?php language_attributes(); ?> class="no-js ie ie6 lte7 lte8 lte9"><![endif]-->
<!--[if IE 7 ]><html <?php language_attributes(); ?> class="no-js ie ie7 lte7 lte8 lte9"><![endif]-->
<!--[if IE 8 ]><html <?php language_attributes(); ?> class="no-js ie ie8 lte8 lte9"><![endif]-->
<!--[if IE 9 ]><html <?php language_attributes(); ?> class="no-js ie ie9 lte9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<title><?php
        if ( is_single() ) { single_post_title(); }
        elseif ( is_home() || is_front_page() ) { bloginfo('name'); print ' | '; bloginfo('description'); get_page_number(); }
        elseif ( is_page() ) { single_post_title(''); }
        elseif ( is_search() ) { bloginfo('name'); print ' | Search results for ' . wp_specialchars($s); get_page_number(); }
        elseif ( is_404() ) { bloginfo('name'); print ' | Not Found'; }
        else { bloginfo('name'); wp_title('|'); get_page_number(); }
    	?></title>
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="apple-touch-icon-144x144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="apple-touch-icon-114x114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="apple-touch-icon-72x72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png">
		<link rel="profile" href="http://gmpg.org/xfn/11" />
		<link rel="stylesheet" href="<?php bloginfo( 'stylesheet_url' ); ?>" /> 
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/normalize.min.css" />
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); wp_head(); ?>
  		<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr-2.0.6.min.js"></script>
  		<script type="text/javascript" src="//use.typekit.net/kdy0fgv.js"></script>
		<script type="text/javascript">try{Typekit.load();}catch(e){}</script>
	</head>
	<body <?php body_class(); ?>>
<div class="header-container">
		<header class="wrapper clearfix">
			<div class="title-container">
				<h1 class="title">Dan Martin</h1>
			</div>
			<div class="follow-container">
			</div>
				<nav><ul>
					<li><a href="">Blog</a></li>
					<li><a href="">About</a></li>
					<li><a href="">Contact</a></li>
				</ul></nav>
  			
		</header>
	</div>
			<div class="main-container">









