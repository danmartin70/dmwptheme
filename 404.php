<?php
/**
 *
 * @package WordPress
 * @subpackage dmresponsive
 * @since DM-Responsive 1.0
 */
?>
<article id="post-0" class="post error404 not-found">
	<header><h1 class="entry-title"><?php _e( 'Not Found', 'hbd-theme' ); ?></h1></header>
	<section class="entry-content">
		<p><?php _e( 'Whoah! You are trying to access some crazy crap right now.  You know, that kind that <em>does not exist</em>.  So stop, take a deep breath, and think a little.  Maybe you can find what you want by searching.  Maybe you made a typo.  As a humble 404 Not Found page, there is only so much I can know.  But I wish you the best of luck at finding the thing you are looking for, whomever you are.', 'hbd-theme' ); ?></p>
		<?php get_search_form(); ?>
	</section><!-- .entry-content -->
</article><!-- #post-0 -->